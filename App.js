import React, { useEffect } from "react";
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import Login from "./modules/Login";
import HomeAdmin from "./modules/HomeAdmin";
import HomeUser from "./modules/HomeUser";
import AppBar from "./components/AppBar";
import Modal from "./components/Modal";
import TopUsers from "./components/TopUsers";

function App() {
  const isLoggedStorage = localStorage.getItem("user") ? true : false;
  const questionsLS = localStorage.getItem("questions") ? localStorage.getItem("questions") : JSON.stringify([]);
  const isLoggedType = localStorage.getItem("user") === "admin" ? "admin" : "user";
  const [isLoggedIn, userLogin] = React.useState(isLoggedStorage);
  const [userType, setUserType] = React.useState(isLoggedType);
  const [modalVisible, isModalVisible] = React.useState(false);
  const [modalContent, setModalContent] = React.useState(<TopUsers />);
  const [questions] = React.useState(JSON.parse(questionsLS));
  const [adminSite, setAdminSite] = React.useState("questions");

  const logout = () => {
    userLogin(false);
    setUserType("user");
    localStorage.removeItem("user");
  };

  const fetchLogin = (user, passwd) => {
    fetch(`http://localhost/php/src/api/login.php`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        user,
        passwd
      })
    })
      .then(response => {
        return response.json();
      })
      .then(res => {
        console.log("BBBBB", res);
        if (res.success) {
          if (res.result === "LOGGED" || res.result === "REGISTERED") {
            localStorage.setItem("user", res.user);
            setUserType(res.type);
            userLogin(true);
          }
          res.result === "WRONG_PASS" && alert("podano zle haslo");
          if (res.result === "QUESTIONS") {
            localStorage.setItem("questions", JSON.stringify(res.questions));
          }
        }
      })
      .catch(err => {
        console.log(err);
      });
  };
  const tryLogin = async (user, passwd) => {
    if (user && passwd) {
      if (passwd.length >= 6 && user !== "admin") {
        fetchLogin(user, passwd);
      } else if (user !== "admin") alert("Hasło musi być o długości 6 lub więcej znaków!");
      else if (user === "admin") fetchLogin(user, passwd);
    }
  };

  useEffect(() => {
    fetchLogin("admin", "quest");
  }, []);

  const showModal = (visibility, content) => {
    isModalVisible(visibility);
    setModalContent(content);
  };

  const udpateAdminSite = e => {
    setAdminSite(e);
  };

  return (
    <Router>
      <Switch>
        <Route path="/login">
          <Login action={tryLogin} isLoggedIn={isLoggedIn} />
        </Route>
        <Route>
          <AppBar logout={logout} toggleModal={showModal} udpateAdminSite={udpateAdminSite} />
          <Modal isVisible={modalVisible} modalContent={modalContent} toggleModal={showModal} />
          {userType === "user" ? <HomeUser questions={questions} /> : <HomeAdmin currentSite={adminSite} />}
          {!isLoggedIn && <Redirect to="/login" />}
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
