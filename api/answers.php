<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "przemtest";

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Content-Type: application/json; charset=utf-8");

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$data = json_decode(file_get_contents('php://input'), true);
$user = $data['user'];
$correct = $data['correct'];
$wrong = $data['wrong'];
$countArray = $data['countArray'];
$i = 0;
while($i < count($countArray)){
    $curr = $countArray[$i];
    $corr = $curr["correct"];
    $wrng = $curr["wrong"];
    $id = $curr["id"];
    $sqlUpdate = "UPDATE questions SET correctCount=correctCount+$corr, wrongCount=wrongCount+$wrng WHERE id='$id'";
    $conn->query($sqlUpdate);
    $i++;
};

$sql = "UPDATE users SET correct=correct+$correct, wrong=wrong+$wrong WHERE user='$user'";
if ($conn->query($sql) === TRUE) {
    echo json_encode("geet");
} else {
    echo json_encode("error");
};

mysqli_close($conn);
?>