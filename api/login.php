<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "przemtest";

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Content-Type: application/json; charset=utf-8");

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

//pytania
$sqlQuestion = "SELECT * FROM questions ORDER BY RAND() LIMIT 10";
$result = mysqli_query($conn, $sqlQuestion);
$jsonQuestions = array();
if (mysqli_num_rows($result) > 0) {
    while($row = mysqli_fetch_assoc($result)) {
        $question = array(
            'id' => $row['id'],
            'question' => $row['question'],
            'answer0' => $row['answer0'],
            'answer1' => $row['answer1'],
            'answer2' => $row['answer2'],
            'answer3' => $row['answer3'],
            'correct' => $row['correct'],
        );
        array_push($jsonQuestions, $question);
    }
};

//logowanie
$data = json_decode(file_get_contents('php://input'), true);
$user=$data['user'];
$pass=$data['passwd'];

$sql = "SELECT * FROM users WHERE user='$user'";
$result = mysqli_query($conn, $sql);
if (mysqli_num_rows($result) > 0) {
    if($pass!=="quest"){
        $row = mysqli_fetch_assoc($result);
        if (password_verify($pass, $row['passwd'])) {
            echo json_encode(array(
                'success'=>true,
                'result'=>'LOGGED',
                'id'=>$row['id'],
                'user'=>$row['user'],
                'type'=>$row['type'],
                'correct'=>$row['correct'],
                'wrong'=>$row['wrong'],
                'questions'=>$jsonQuestions,
            ));
        } else {
            echo json_encode(array(
                'success'=>true,
                'result'=>'WRONG_PASS',
            ));
        }
    } else {
        echo json_encode(array(
            'success'=>true,
            'result'=>'QUESTIONS',
            'questions'=>$jsonQuestions,
        ));
    }
} else {
    $hashed = password_hash($pass, PASSWORD_DEFAULT);
    if($user){
        $sql = "INSERT INTO users (user, passwd) VALUES ('$user', '$hashed')";
        if ($conn->query($sql) === TRUE) {
            $sql = "SELECT * FROM users WHERE user='$user' AND passwd='$hashed'";
            $result = mysqli_query($conn, $sql);
            $row = mysqli_fetch_assoc($result);
            echo json_encode(array(
                'success'=>true,
                'result'=>'REGISTERED',
                'id'=>$row['id'],
                'user'=>$row['user'],
                'type'=>$row['type'],
                'correct'=>$row['correct'],
                'wrong'=>$row['wrong'],
                'questions'=>$jsonQuestions,
            ));
        } else {
            echo json_encode(array(
                'success'=>false,
                'result'=>"ErrorN: " . $sql . "<br>" . $conn->error,
            ));
        }
    }
}

// print_r($_GET);
mysqli_close($conn);
?>