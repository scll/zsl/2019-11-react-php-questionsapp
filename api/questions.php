<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "przemtest";

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Content-Type: application/json; charset=utf-8");

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

//pytania
$sqlQuestion = "SELECT * FROM questions";
$result = mysqli_query($conn, $sqlQuestion);
$jsonQuestions = array();
if (mysqli_num_rows($result) > 0) {
    while($row = mysqli_fetch_assoc($result)) {
        $question = array(
            'id' => $row['id'],
            'question' => $row['question'],
            'answer0' => $row['answer0'],
            'answer1' => $row['answer1'],
            'answer2' => $row['answer2'],
            'answer3' => $row['answer3'],
            'correct' => $row['correct'],
            'correctCount' => $row['correctCount'],
            'wrongCount' => $row['wrongCount'],
        );
        array_push($jsonQuestions, $question);
    }
};

echo json_encode(array(
    'success'=>true,
    'result'=>'QUESTIONS_DOWNLOADED',
    'questions'=>$jsonQuestions,
));

mysqli_close($conn);
?>