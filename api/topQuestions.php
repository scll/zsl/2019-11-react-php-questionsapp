<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "przemtest";

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Content-Type: application/json; charset=utf-8");

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$json = array();

$sql = "SELECT * FROM questions WHERE (correctCount=0) AND (wrongCount!=0) ORDER BY (wrongCount) DESC";
$result = mysqli_query($conn, $sql);
if (mysqli_num_rows($result) > 0) {
    while($row = mysqli_fetch_assoc($result)) {
        $question = array(
            'id' => $row['id'],
            'question' => $row['question'],
            'correctCount' => $row['correctCount'],
            'wrongCount' => $row['wrongCount'],
        );
        array_push($json, $question);
    };
};

$sql = "SELECT * FROM questions WHERE (correctCount!=0) ORDER BY (wrongCount/correctCount) DESC";
$result = mysqli_query($conn, $sql);
if (mysqli_num_rows($result) > 0) {
    while($row = mysqli_fetch_assoc($result)) {
        $question = array(
            'id' => $row['id'],
            'question' => $row['question'],
            'correctCount' => $row['correctCount'],
            'wrongCount' => $row['wrongCount'],
        );
        array_push($json, $question);
    };
};
$jsonSliced = array_slice($json, 0, 10);
echo json_encode(array(
    'success'=>true,
    'result'=>'DONE',
    'questions'=>$jsonSliced,
));

mysqli_close($conn);
?>