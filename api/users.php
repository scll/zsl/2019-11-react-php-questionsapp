<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "przemtest";

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Content-Type: application/json; charset=utf-8");

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

//pytania
$sqlQuestion = "SELECT * FROM users WHERE users.type!='admin'";
$result = mysqli_query($conn, $sqlQuestion);
$json = array();
if (mysqli_num_rows($result) > 0) {
    while($row = mysqli_fetch_assoc($result)) {
        $question = array(
            'id' => $row['id'],
            'user' => $row['user'],
            'correct' => $row['correct'],
            'wrong' => $row['wrong'],
        );
        array_push($json, $question);
    };
};

echo json_encode(array(
    'success'=>true,
    'result'=>'QUESTIONS_DOWNLOADED',
    'users'=>$json,
));

mysqli_close($conn);
?>