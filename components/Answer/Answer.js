import React from "react";
import "./style.css";
import { Grid } from "@material-ui/core";

const Component = ({ answer, letter, checked, correctAnswer, isCompleted, checkFunc }) => {
  let cName;
  if (checked && correctAnswer) cName = "mainCont done correct";
  if (checked && !correctAnswer) cName = "mainCont done wrong";
  if (!checked && correctAnswer) cName = "mainCont done correct";
  if (!checked && !correctAnswer) cName = "mainCont done";
  return isCompleted ? (
    <Grid className={cName} container direction="row" justify="space-around" alignItems="center">
      <Grid className={checked ? "answerCheckCont checked" : "answerCheckCont"} item>
        {letter}
      </Grid>
      <Grid className="answerTextCont" item>
        {answer}
      </Grid>
    </Grid>
  ) : (
    <Grid className="mainCont" container direction="row" justify="space-around" alignItems="center" onClick={() => checkFunc()}>
      <Grid className={checked ? "answerCheckCont checked" : "answerCheckCont"} item>
        {letter}
      </Grid>
      <Grid className="answerTextCont" item>
        {answer}
      </Grid>
    </Grid>
  );
};

export default Component;
