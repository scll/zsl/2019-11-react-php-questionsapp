import React from "react";
import "./style.css";
import { Paper, Grid, Container } from "@material-ui/core";
import Answer from "../Answer";

const Component = ({ question, answerChecked, mainIndex, checkFunc, isCompleted }) => {
  const check = i => {
    checkFunc(mainIndex, i);
  };
  const answersArray = [
    {
      answer: question.answer0,
      letter: "A"
    },
    {
      answer: question.answer1,
      letter: "B"
    },
    {
      answer: question.answer2,
      letter: "C"
    },
    {
      answer: question.answer3,
      letter: "D"
    }
  ];
  return (
    <Container>
      <Grid item xs={12} md={8} lg={7} style={{ margin: "0 auto" }}>
        <Paper className="qGroupPaper">
          <Grid container direction="column" justify="center" alignItems="center">
            <Grid item className="questionText">
              {mainIndex + 1}. {question.question}
            </Grid>
            {answersArray.map((e, index) => (
              <Answer
                answer={e.answer}
                letter={e.letter}
                checkFunc={() => check(index)}
                correctAnswer={JSON.parse(question.correct) === index}
                isCompleted={isCompleted}
                checked={index === answerChecked}
                key={index}
              />
            ))}
          </Grid>
        </Paper>
      </Grid>
    </Container>
  );
};

export default Component;
