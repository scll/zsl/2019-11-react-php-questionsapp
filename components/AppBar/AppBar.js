import React from "react";
import { AppBar, Toolbar, Typography, IconButton } from "@material-ui/core";
// import AccountCircle from "@material-ui/icons/AccountCircle";
import StarsRoundedIcon from "@material-ui/icons/StarsRounded";
import ExitToAppRoundedIcon from "@material-ui/icons/ExitToAppRounded";
import FormatListNumberedRoundedIcon from "@material-ui/icons/FormatListNumberedRounded";
import SupervisorAccountRoundedIcon from "@material-ui/icons/SupervisorAccountRounded";
import AssessmentRoundedIcon from "@material-ui/icons/AssessmentRounded";
import "./style.css";
import TopUsers from "../TopUsers";
import TopQuestions from "../TopQuestions";

export default function MAppBar({ toggleModal, acc, logout, udpateAdminSite }) {
  return localStorage.getItem("user") !== "admin" ? (
    <>
      <AppBar position="sticky" className="appbar">
        <Toolbar className="toolbar">
          <div>
            <Typography variant="h6">Zalogowano jako: {localStorage.getItem("user")}</Typography>
          </div>
          <div>
            <IconButton onClick={() => toggleModal(true, <TopUsers />)} color="inherit">
              <StarsRoundedIcon />
            </IconButton>
            {/* <IconButton onClick={() => acc()} color="inherit">
              <AccountCircle />
            </IconButton> */}
            <IconButton onClick={() => logout()} color="inherit">
              <ExitToAppRoundedIcon style={{ fill: "#e6ccff" }} />
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
    </>
  ) : (
    <>
      <AppBar position="sticky" className="appbar">
        <Toolbar className="toolbar">
          <div>
            <Typography variant="h6">Zalogowano jako: {localStorage.getItem("user")}</Typography>
          </div>
          <div>
            <IconButton onClick={() => udpateAdminSite("questions")} color="inherit">
              <FormatListNumberedRoundedIcon />
            </IconButton>
            <IconButton onClick={() => udpateAdminSite("users")} color="inherit">
              <SupervisorAccountRoundedIcon />
            </IconButton>
            <IconButton onClick={() => toggleModal(true, <TopQuestions />)} color="inherit">
              <AssessmentRoundedIcon />
            </IconButton>
            <IconButton onClick={() => toggleModal(true, <TopUsers />)} color="inherit">
              <StarsRoundedIcon />
            </IconButton>
            <IconButton onClick={() => logout()} color="inherit">
              <ExitToAppRoundedIcon style={{ fill: "#e6ccff" }} />
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
    </>
  );
}
