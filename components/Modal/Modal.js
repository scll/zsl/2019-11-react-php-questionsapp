import React from "react";
import { Modal, Grid, Backdrop, Fade } from "@material-ui/core";
import "./style.css";

export default function TransitionsModal({ modalContent, isVisible, toggleModal }) {
  return (
    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={"modal"}
        open={isVisible}
        onClose={() => toggleModal(false, null)}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500
        }}
      >
        <Fade in={isVisible}>
          <Grid item xs={12} sm={8} className={"container"}>
            {modalContent}
          </Grid>
        </Fade>
      </Modal>
    </div>
  );
}
