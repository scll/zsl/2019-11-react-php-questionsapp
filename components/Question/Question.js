import React from "react";
import "./style.css";
import { Paper, Grid, Container, Button, TextField } from "@material-ui/core";

const Component = ({ questionData, add }) => {
  const [DATA, setDATA] = React.useState(questionData);
  const [question, updateQuestion] = React.useState(DATA.question);
  const [answer0, updateAnswer0] = React.useState(DATA.answer0);
  const [answer1, updateAnswer1] = React.useState(DATA.answer1);
  const [answer2, updateAnswer2] = React.useState(DATA.answer2);
  const [answer3, updateAnswer3] = React.useState(DATA.answer3);
  const answArr = [answer0, answer1, answer2, answer3];
  const funcArr = [updateAnswer0, updateAnswer1, updateAnswer2, updateAnswer3];
  const [correctCount] = React.useState(DATA.correctCount);
  const [wrongCount] = React.useState(DATA.wrongCount);
  const [correct, updateCorrect] = React.useState(DATA.correct);
  const [isEdited, isCurrentlyEdited] = React.useState(false);
  const [visible, isVisible] = React.useState(true);
  // console.log("MMMM", DATA);
  const fetchQuestion = () => {
    fetch(`http://localhost/php/src/api/updateQuestion.php`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        id: DATA.id,
        question,
        answer0,
        answer1,
        answer2,
        answer3,
        correct
      })
    })
      .then(response => {
        return response.json();
      })
      .then(res => {
        console.log("BBBBB", res);
      })
      .catch(err => {
        console.log(err);
      });
  };
  const fetchAdd = () => {
    fetch(`http://localhost/php/src/api/addQuestion.php`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        question,
        answer0,
        answer1,
        answer2,
        answer3,
        correct
      })
    })
      .then(response => {
        return response.json();
      })
      .then(res => {
        console.log("BBBBB", res);
        const newData = {
          answer0: "",
          answer1: "",
          answer2: "",
          answer3: "",
          correct: "0",
          question: "",
          correctCount: "",
          id: "",
          wrongCount: ""
        };
        setDATA(newData);
        updateCorrect(DATA.correct);
        updateAnswer0(DATA.answer0);
        updateAnswer1(DATA.answer1);
        updateAnswer2(DATA.answer2);
        updateAnswer3(DATA.answer3);
        updateQuestion(DATA.question);
        alert("Dodano do bazy.");
      })
      .catch(err => {
        console.log(err);
      });
  };
  const fetchRemove = () => {
    fetch(`http://localhost/php/src/api/removeQuestion.php`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        id: DATA.id
      })
    })
      .then(response => {
        return response.json();
      })
      .then(res => {
        console.log("BBBBB", res);
        isVisible(false);
      })
      .catch(err => {
        console.log(err);
      });
  };
  console.log(question, answer0, answer1, answer2, answer3, correct);
  const answersArray = [
    {
      answer: answer0,
      letter: "A"
    },
    {
      answer: answer1,
      letter: "B"
    },
    {
      answer: answer2,
      letter: "C"
    },
    {
      answer: answer3,
      letter: "D"
    }
  ];
  const options = answersArray.map((e, index) => (
    <Container key={index}>
      <Grid
        className={
          JSON.parse(correct) === index ? "mainCont2 corr" : "mainCont2"
        }
        container
        direction="row"
        justify="space-around"
        alignItems="center"
      >
        <Grid className={""} item>
          {e.letter}
        </Grid>
        <Grid
          className="answerTextCont"
          item
          style={{ overflow: "hidden", textOverflow: "ellipsis" }}
        >
          {e.answer}
        </Grid>
      </Grid>
    </Container>
  ));
  const optionsEditable = answersArray.map((e, index) => (
    <Container key={index}>
      <Grid
        className={
          JSON.parse(correct) === index ? "mainCont2 corr" : "mainCont2"
        }
        container
        direction="row"
        justify="space-around"
        alignItems="center"
      >
        <Grid
          className={
            correct === index ? "answerCheckCont2 checked" : "answerCheckCont2"
          }
          item
          onClick={() => {
            updateCorrect(index);
          }}
        >
          {e.letter}
        </Grid>
        <Grid className="answerTextCont" item>
          <TextField
            className="styledTextField2"
            value={answArr[index]}
            onChange={e => funcArr[index](e.target.value)}
            type="text"
            variant="outlined"
            fullWidth
          />
        </Grid>
      </Grid>
    </Container>
  ));
  //przed edytowaniem
  if (!isEdited && !add && visible)
    return (
      <Container>
        <Grid item xs={12} md={8} lg={7} style={{ margin: "0 auto" }}>
          <Paper className="qGroupPaper">
            <Grid
              container
              direction="column"
              justify="center"
              alignItems="center"
            >
              <Grid
                container
                direction="row"
                justify="space-between"
                alignItems="center"
              >
                <Grid
                  xs={6}
                  item
                  container
                  direction="row"
                  justify="space-between"
                  alignItems="center"
                >
                  <Grid item style={{ fontSize: "1.8vh" }}>
                    Poprawne odp.: {correctCount}
                  </Grid>
                  <Grid item style={{ fontSize: "1.8vh" }}>
                    Niepopr. odp.: {wrongCount}
                  </Grid>
                </Grid>
                <Grid
                  xs={5}
                  item
                  container
                  direction="row"
                  justify="flex-end"
                  alignItems="center"
                >
                  <Button
                    className="styledButton2"
                    variant="contained"
                    onClick={() => {
                      fetchRemove();
                    }}
                  >
                    Usuń
                  </Button>
                  <Button
                    className="styledButton2"
                    variant="contained"
                    onClick={() => {
                      isCurrentlyEdited(true);
                    }}
                  >
                    Edytuj
                  </Button>
                </Grid>
              </Grid>
              <Grid item className="questionText2">
                {question}
              </Grid>
              {options}
            </Grid>
          </Paper>
        </Grid>
      </Container>
    );
  //w trakcie edytowania
  else if (isEdited && !add && visible)
    return (
      <Container>
        <Grid item xs={12} md={8} lg={7} style={{ margin: "0 auto" }}>
          <Paper className="qGroupPaper">
            <Grid
              container
              direction="column"
              justify="center"
              alignItems="center"
            >
              <Grid
                container
                direction="row"
                justify="space-between"
                alignItems="center"
              >
                <Grid
                  xs={6}
                  item
                  container
                  direction="row"
                  justify="space-between"
                  alignItems="center"
                >
                  <Grid item style={{ fontSize: "1.8vh" }}>
                    Poprawne odp.: {correctCount}
                  </Grid>
                  <Grid item style={{ fontSize: "1.8vh" }}>
                    Niepopr. odp.: {wrongCount}
                  </Grid>
                </Grid>
                <Grid
                  xs={5}
                  item
                  container
                  direction="row"
                  justify="flex-end"
                  alignItems="center"
                >
                  <Button
                    className="styledButton2"
                    variant="contained"
                    onClick={() => {
                      isCurrentlyEdited(false);
                      fetchQuestion();
                      const newData = {
                        answer0,
                        answer1,
                        answer2,
                        answer3,
                        correct,
                        correctCount,
                        id: DATA.id,
                        question,
                        wrongCount
                      };
                      setDATA(newData);
                    }}
                  >
                    Zapisz
                  </Button>
                  <Button
                    className="styledButton2"
                    variant="contained"
                    onClick={() => {
                      isCurrentlyEdited(false);
                      updateCorrect(DATA.correct);
                      updateAnswer0(DATA.answer0);
                      updateAnswer1(DATA.answer1);
                      updateAnswer2(DATA.answer2);
                      updateAnswer3(DATA.answer3);
                    }}
                  >
                    Anuluj
                  </Button>
                </Grid>
              </Grid>
              <Grid container item>
                <TextField
                  className="styledTextField2 questionEditable"
                  value={question}
                  onChange={e => updateQuestion(e.target.value)}
                  type="text"
                  variant="outlined"
                  fullWidth
                  multiline
                />
              </Grid>
              {optionsEditable}
            </Grid>
          </Paper>
        </Grid>
      </Container>
    );
  //dodawanie
  else if (add && visible)
    return (
      <Container>
        <Grid item xs={12} md={8} lg={7} style={{ margin: "0 auto" }}>
          <Paper className="qGroupPaper">
            <Grid
              container
              direction="column"
              justify="center"
              alignItems="center"
            >
              <Grid
                container
                direction="row"
                justify="space-between"
                alignItems="center"
              >
                <Grid
                  xs={6}
                  item
                  container
                  direction="row"
                  justify="space-between"
                  alignItems="center"
                >
                  <Grid item style={{ fontSize: "2.2vh" }}>
                    Dodaj kolejne pytanie:
                  </Grid>
                </Grid>
                <Grid
                  xs={5}
                  item
                  container
                  direction="row"
                  justify="flex-end"
                  alignItems="center"
                >
                  <Button
                    className="styledButton2"
                    variant="contained"
                    onClick={() => {
                      if (
                        answer0 &&
                        answer1 &&
                        answer2 &&
                        answer3 &&
                        question
                      ) {
                        fetchAdd();
                      } else {
                        alert("Uzupełnij wszystkie pola!");
                      }
                    }}
                  >
                    Dodaj
                  </Button>
                </Grid>
              </Grid>
              <Grid container item>
                <TextField
                  className="styledTextField2 questionEditable"
                  value={question}
                  onChange={e => updateQuestion(e.target.value)}
                  type="text"
                  variant="outlined"
                  fullWidth
                  multiline
                />
              </Grid>
              {optionsEditable}
            </Grid>
          </Paper>
        </Grid>
      </Container>
    );
  else return null;
};

export default Component;
