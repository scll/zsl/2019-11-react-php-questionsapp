import React from "react";
import "./style.css";
import { Grid, Typography } from "@material-ui/core";

const Component = () => {
  const [questions, setQuestions] = React.useState([]);
  const downloadData = () => {
    fetch(`http://localhost/php/src/api/topQuestions.php`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        return response.json();
      })
      .then(res => {
        console.log(res);
        setQuestions(res.questions);
      })
      .catch(err => {
        console.log(err);
      });
  };
  React.useEffect(() => {
    downloadData();
  }, []);
  if (questions.length < 10) {
    for (let i = questions.length; i < 10; i++) {
      questions.push({ id: "x", question: "----", correctCount: "0", wrongCount: "0" });
    }
  }
  const questionsMap = questions.map((e, index) => (
    <Grid container item direction="row" justify="space-between" alignItems="center" key={index} className="cellRow">
      <Grid item xs={1} className={"cell header"}>
        {index + 1}.
      </Grid>
      <Grid item xs={7} className={"cell"}>
        {e.question}
      </Grid>
      <Grid item xs={1} className={"cell correct"}>
        {e.correctCount}
      </Grid>
      <Grid item xs={1} className={"cell wrong"}>
        {e.wrongCount}
      </Grid>
      <Grid item xs={1} className={"cell"}>
        {e.wrongCount !== "0"
          ? Math.round((JSON.parse(e.correctCount) / (JSON.parse(e.correctCount) + JSON.parse(e.wrongCount))) * 100)
          : e.correctCount === "0"
          ? 0
          : 100}
        %
      </Grid>
      <Grid item xs={1} className={"cell"}>
        {e.id}
      </Grid>
    </Grid>
  ));
  return (
    <Grid container direction="column" justify="center" alignItems="center" className={"container2"}>
      <Typography variant="h4" style={{ textAlign: "center", marginBottom: "15px" }}>
        Najtrudniejsze pytania
      </Typography>
      <Grid container item direction="row" justify="space-between" alignItems="center">
        <Grid item xs={1} className={"cell header"}>
          lp.
        </Grid>
        <Grid item xs={7} className={"cell header"}>
          Pytanie
        </Grid>
        <Grid item xs={1} className={"cell header"}>
          Poprawne
        </Grid>
        <Grid item xs={1} className={"cell header"}>
          Błędne
        </Grid>
        <Grid item xs={1} className={"cell header"}>
          Wynik(%)
        </Grid>
        <Grid item xs={1} className={"cell header"}>
          ID
        </Grid>
      </Grid>
      {questionsMap}
    </Grid>
  );
};

export default Component;
