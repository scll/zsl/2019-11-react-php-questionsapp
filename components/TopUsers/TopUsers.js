import React from "react";
import "./style.css";
import { Grid, Typography } from "@material-ui/core";

const Component = () => {
  const [users, setUsers] = React.useState([]);
  const downloadData = () => {
    fetch(`http://localhost/php/src/api/topUsers.php`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        return response.json();
      })
      .then(res => {
        console.log(res);
        setUsers(res.users);
      })
      .catch(err => {
        console.log(err);
      });
  };
  React.useEffect(() => {
    downloadData();
  }, []);
  if (users.length < 10) {
    for (let i = users.length; i < 10; i++) {
      users.push({ id: "x", user: "----", correct: "0", wrong: "0" });
    }
  }
  const usersMap = users.map((e, index) => (
    <Grid container item direction="row" justify="space-between" alignItems="center" key={index} className="cellRow">
      <Grid item xs={1} className={"cell header"}>
        {index + 1}.
      </Grid>
      <Grid item xs={5} className={"cell"}>
        {e.user}
      </Grid>
      <Grid item xs={2} className={"cell correct"}>
        {e.correct}
      </Grid>
      <Grid item xs={2} className={"cell wrong"}>
        {e.wrong}
      </Grid>
      <Grid item xs={2} className={"cell"}>
        {e.wrong !== "0" ? Math.round((JSON.parse(e.correct) / (JSON.parse(e.correct) + JSON.parse(e.wrong))) * 100) : e.correct === "0" ? 0 : 100}%
      </Grid>
    </Grid>
  ));
  return (
    <Grid container direction="column" justify="center" alignItems="center" className={"container2"}>
      <Typography variant="h4" style={{ textAlign: "center", marginBottom: "15px" }}>
        Najlepsi użytkownicy
      </Typography>
      <Grid container item direction="row" justify="space-between" alignItems="center">
        <Grid item xs={1} className={"cell header"}>
          lp.
        </Grid>
        <Grid item xs={5} className={"cell header"}>
          Nick
        </Grid>
        <Grid item xs={2} className={"cell header"}>
          Poprawne
        </Grid>
        <Grid item xs={2} className={"cell header"}>
          Błędne
        </Grid>
        <Grid item xs={2} className={"cell header"}>
          Wynik %
        </Grid>
      </Grid>
      {usersMap}
    </Grid>
  );
};

export default Component;
