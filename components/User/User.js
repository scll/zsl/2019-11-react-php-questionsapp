import React from "react";
import "./style.css";
import { Paper, Grid, Container, Button } from "@material-ui/core";

const Component = ({ userData }) => {
  const [isVisible, setVisibility] = React.useState(true);
  const delUser = () => {
    fetch(`http://localhost/php/src/api/delUser.php`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        id: userData.id
      })
    })
      .then(response => {
        return response.json();
      })
      .then(res => {
        console.log("BBBBB", res);
      })
      .catch(err => {
        console.log(err);
      });
  };
  return isVisible ? (
    <Container>
      <Grid item xs={12} sm={8} md={5} style={{ margin: "0 auto" }}>
        <Paper className="qGroupPaper">
          <Grid container direction="column" justify="center" alignItems="center">
            <Grid container direction="row" justify="space-between" alignItems="center">
              <Grid>
                <Grid item style={{ fontSize: "2.6vh", margin: "5px", fontWeight: "bold" }}>
                  {userData.user}
                </Grid>
                <Grid item style={{ fontSize: "1.8vh", margin: "5px" }}>
                  Poprawne odp.: <b style={{ color: "#007e13" }}>{userData.correct}</b>
                </Grid>
                <Grid item style={{ fontSize: "1.8vh", margin: "5px" }}>
                  Niepopr. odp.: <b style={{ color: "#fa7474" }}>{userData.wrong}</b>
                </Grid>
              </Grid>
              <Grid xs={3} item container direction="row" justify="flex-end" alignItems="center">
                <Button
                  className="styledButton2"
                  variant="contained"
                  onClick={() => {
                    if (window.confirm("Na pewno chcesz usunąć?")) {
                      setVisibility(false);
                      delUser();
                    }
                  }}
                >
                  Usuń
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
    </Container>
  ) : null;
};

export default Component;
