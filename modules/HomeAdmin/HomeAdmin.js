import React from "react";
import "./style.css";
import { Container } from "@material-ui/core";
import Question from "../../components/Question";
import User from "../../components/User";

const Component = ({ currentSite }) => {
  const [questions, setQuestions] = React.useState([]);
  const [users, setUsers] = React.useState([]);
  const fetchData = () => {
    fetch(`http://localhost/php/src/api/questions.php`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        return response.json();
      })
      .then(res => {
        console.log("QUESTIONS ADMIN: ", res);
        setQuestions(res.questions);
      })
      .catch(err => {
        console.log(err);
      });
    fetch(`http://localhost/php/src/api/users.php`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        return response.json();
      })
      .then(res => {
        console.log("USERS ADMIN: ", res);
        setUsers(res.users);
      })
      .catch(err => {
        console.log(err);
      });
  };
  React.useEffect(() => {
    fetchData();
  }, []);

  const questionsMap = questions.map((e, index) => (
    <Question questionData={e} key={index} />
  ));
  const usersMap = users.map((e, index) => <User userData={e} key={index} />);
  const blank = {
    answer0: "",
    answer1: "",
    answer2: "",
    answer3: "",
    correct: "0",
    question: ""
  };
  if (currentSite === "users") {
    return <Container>{usersMap}</Container>;
  } else if (currentSite === "questions") {
    return (
      <Container>
        <Question add={true} questionData={blank} />
        {questionsMap}
      </Container>
    );
  } else return null;
};

export default Component;
