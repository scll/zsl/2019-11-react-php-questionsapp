import React from "react";
import "./style.css";
import { Grid, Container, Button } from "@material-ui/core";
import QuestionGroup from "../../components/AnswersGroup";

const Component = ({ questions }) => {
  const [answers, answersUpdate] = React.useState([4, 4, 4, 4, 4, 4, 4, 4, 4, 4]);
  const [testStarted, isTestStarted] = React.useState(false);
  const [testDone, isTestDone] = React.useState(false);
  const [corr, setCorr] = React.useState(null);
  const [wrng, setWrng] = React.useState(null);
  const answersUpd = (i, checkedAnswer) => {
    const m = [...answers];
    m[i] = checkedAnswer;
    answersUpdate(m);
  };

  const questionsMap = questions.map((e, index) => (
    <QuestionGroup question={e} answerChecked={answers[index]} mainIndex={index} checkFunc={answersUpd} key={index} isCompleted={testDone} />
  ));
  const sendAnswers = () => {
    if (answers.indexOf(4) === -1) {
      if (window.confirm("Czy na pewno chcesz wysłać odpowiedzi?")) {
        let correct = 0;
        let wrong = 0;
        let countArray = [];
        answers.forEach((e, index) => {
          console.log(e, questions[index].correct);
          if (e === JSON.parse(questions[index].correct)) {
            let obj = {
              id: questions[index].id,
              correct: 1,
              wrong: 0
            };
            correct++;
            countArray.push(obj);
          } else {
            let obj = {
              id: questions[index].id,
              correct: 0,
              wrong: 1
            };
            wrong++;
            countArray.push(obj);
          }
        });
        fetch(`http://localhost/php/src/api/answers.php`, {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
          },
          body: JSON.stringify({
            user: localStorage.getItem("user"),
            correct,
            wrong,
            countArray
          })
        })
          .then(response => {
            return response.json();
          })
          .then(res => {
            console.log(res);
            setCorr(correct);
            setWrng(wrong);
            window.scrollTo(0, 0);
            isTestDone(true);
          })
          .catch(err => {
            console.log(err);
          });
      }
    } else {
      window.alert("Zaznacz wszystkie odpowiedzi!");
    }
  };

  return testStarted ? (
    <Container>
      <Grid container style={{ width: "100%" }} justify="center" alignItems="center">
        {testDone ? (
          <div style={{ width: "100%", textAlign: "center" }}>
            Poprawne: {corr}, Niepoprawne: {wrng}
          </div>
        ) : null}
        {questionsMap}
        <Button
          className="styledButton"
          variant="contained"
          onClick={() => {
            testDone ? window.location.reload() : sendAnswers();
          }}
        >
          {!testDone ? "WYŚLIJ ODPOWIEDZI" : "ZACZNIJ OD NOWA"}
        </Button>
      </Grid>
    </Container>
  ) : (
    <Container>
      <Grid container style={{ height: "90vh" }} justify="center" alignItems="center">
        <Button className="styledButton" variant="contained" onClick={() => isTestStarted(true)}>
          Zacznij test
        </Button>
      </Grid>
    </Container>
  );
};

export default Component;
