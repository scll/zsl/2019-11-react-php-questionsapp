import React from "react";
import { withRouter, Redirect } from "react-router";
import "./style.css";
import { Paper, TextField, Grid, Container, Button } from "@material-ui/core";

const Login = ({ action, isLoggedIn }) => {
  const [mailInput, changeMailInput] = React.useState("");
  const [passInput, changePassInput] = React.useState("");

  if (isLoggedIn) return <Redirect to={"/"} />;

  return (
    <Container>
      <Grid container style={{ minHeight: "100vh" }} justify="center" alignItems="center">
        <Grid item xs={12} sm={7} md={6} lg={6}>
          <Paper className="formBox">
            <Grid container direction="column" justify="center" alignItems="center" style={{ width: "75%", margin: "0 auto" }}>
              <TextField
                className="styledTextField"
                value={mailInput}
                onChange={e => changeMailInput(e.target.value)}
                label="Nick"
                type="text"
                variant="outlined"
                fullWidth
              />
              <TextField
                className="styledTextField"
                value={passInput}
                onChange={e => changePassInput(e.target.value)}
                label="Hasło"
                type="password"
                variant="outlined"
                fullWidth
              />
              <Button
                className="styledButton"
                variant="contained"
                fullWidth
                onClick={() => {
                  action(mailInput, passInput);
                }}
              >
                Login
              </Button>
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    </Container>
  );
};

export default withRouter(Login);
