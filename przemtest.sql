-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 08 Gru 2019, 15:49
-- Wersja serwera: 10.4.8-MariaDB
-- Wersja PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `przemtest`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `question` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `answer0` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `answer1` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `answer2` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `answer3` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `correct` int(11) NOT NULL,
  `correctCount` int(11) NOT NULL DEFAULT 0,
  `wrongCount` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `questions`
--

INSERT INTO `questions` (`id`, `question`, `answer0`, `answer1`, `answer2`, `answer3`, `correct`, `correctCount`, `wrongCount`) VALUES
(1, 'Wartość i typ zmiennej w języku PHP można sprawdzić za pomocą funkcji', 'var_dump()', 'readfile()', 'implode()', 'strlen()', 0, 1, 6),
(2, 'Saturacja koloru nazywana jest inaczej', 'nasyceniem koloru', 'dopełnieniem koloru', 'jasnością koloru', 'przezroczystością koloru', 0, 0, 6),
(3, 'Którą klauzulę powinno się zastosować w poleceniu CREATE TABLE języka SQL, aby dane pole rekordu nie było puste?', 'CHECK', 'NOT NULL', 'DEFAULT', 'NULL', 1, 2, 5),
(4, 'W języku SQL klauzula DISTINCT instrukcji SELECT sprawi, że zwrócone dane', 'będą spełniały określony warunek', 'zostaną posortowane', 'będą pogrupowane według określonego pola', 'nie będą zawierały powtórzeń', 3, 6, 0),
(5, 'Jak to jest być skrybą, dobrze?', 'Nie ma tak, że dobrze albo że nie dobrze', 'Marchew', 'Praca społeczna', 'Dobrze', 0, 3, 4),
(6, 'Do grupowania obszarów na poziomie bloków, które będą formatowane za pośrednictwem znaczników, należy użyć', '<div>', '<span>', '<a>', '<code>', 0, 0, 4),
(7, 'Które polecenie SQL zamieni w tabeli tab w kolumnie kol wartość Ania na Zosia?', 'UPDATE tab SET kol=\'Ania\' WHERE kol=\'Zosia\';', 'UPDATE tab SET kol=\'Zosia\' WHERE kol=\'Ania\';', 'ALTER TABLE tab CHANGE kol=\'Ania\' kol=\'Zosia\';', 'ALTER TABLE tab CHANGE kol=\'Zosia\' kol=\'Ania\';', 1, 1, 2),
(8, 'W języku HTML atrybut alt znacznika img jest wykorzystywany w celu zdefiniowania', 'ścieżki i nazwy pliku źródłowego grafiki', 'atrybutów grafiki, takich jak rozmiar, obramowanie, wyrównanie', 'podpisu, który zostanie wyświetlony pod grafiką', 'tekstu, który będzie wyświetlony, jeśli nie może być wyświetlona grafika', 3, 0, 1),
(9, 'W programowaniu obiektowym mechanizm współdzielenia pól i metod klasy w taki sposób, że klasa pochodna zawiera metody zdefiniowane w klasie bazowej nazywa się', 'hermetyzacją', 'dziedziczeniem', 'polimorfizmem', 'wirtualizacją', 1, 0, 5),
(10, 'W języku JavaScript, aby sprawdzić warunek czy liczba znajduje się w przedziale (100;200>, należy zapisać:', 'if (liczba > 100 || liczba <= 200)', 'if (liczba < 100 || liczba >= 200)', 'if (liczba > 100 && liczba <= 200)', 'if (liczba < 100 && liczba <= 200)', 2, 0, 6),
(11, 'W MS SQL Server polecenie RESTORE DATABASE służy do', 'odtworzenia bazy danych z kopii bezpieczeństwa', 'odświeżenia bazy danych z kontrolą więzów integralności', 'przebudowania bazy danych w oparciu o buforowane dane', 'usunięcia bazy danych z serwera centralnego subskrybenta', 0, 0, 3),
(12, 'Narzędziem służącym do grupowania i prezentowania informacji z wielu rekordów w celu ich drukowania lub rozpowszechniania jest', 'makropolecenie', 'kwerenda', 'raport', 'prof. Skauba', 2, 2, 5),
(13, 'Jaki kierunek na studiach ukończył Grzegorz Adam Zelcer?', 'informatyka stosowana', 'matematyka', 'urbanistyka', 'strzelanie i kobiety', 3, 2, 5),
(14, 'Jak na imię miał Adam Mickiewicz?', 'Adam', 'Juliusz', 'Maciej', 'Piotr', 1, 1, 2);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user` varchar(20) COLLATE utf8_polish_ci NOT NULL,
  `passwd` mediumtext COLLATE utf8_polish_ci NOT NULL,
  `type` varchar(10) COLLATE utf8_polish_ci NOT NULL DEFAULT 'user',
  `correct` int(11) NOT NULL DEFAULT 0,
  `wrong` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `user`, `passwd`, `type`, `correct`, `wrong`) VALUES
(5, 'admin', '$2y$10$BeJVMsJjh/8Dxd.fgt4yJOA6o/uTLRN5EsLFNcyPAsOmpP5lId2Ly', 'admin', 0, 0),
(17, 'dad', '$2y$10$Ua2xQycQrQnY2zw/Jv35I.8OXQgkr7W7/HfoCX.L.9/VNSFhZcYFy', 'user', 20, 40),
(18, 'test', '$2y$10$YyESf/AMjHevsn17gKAULeNcFAPoEk4anAD00u6SHp79ExqzxWuGG', 'user', 17, 3),
(19, 'test2', '$2y$10$GEuMEAI7qAm4u/RTgwmre.c7MJKOyIkSf3hB8KbJ57dofmB4qpjsa', 'user', 3, 7),
(20, 'test3333', '$2y$10$n9Vr1Leyj7k4tvnIG8Fl3.F30vyIXVm24E9d3ydGkufVSXZlYFb6G', 'user', 5, 15),
(21, 'test44', '$2y$10$GabnzuOoJtulVG0WmeI3zuE0u8zHZgALfkC0LCdHUgR2lamVCHuWS', 'user', 10, 0),
(22, 'test55', '$2y$10$GabnzuOoJtulVG0WmeI3zuE0u8zHZgALfkC0LCdHUgR2lamVCHuWS', 'user', 20, 1),
(23, 'test6', '$2y$10$GabnzuOoJtulVG0WmeI3zuE0u8zHZgALfkC0LCdHUgR2lamVCHuWS', 'user', 30, 0);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
